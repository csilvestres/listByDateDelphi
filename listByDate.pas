unit listByDate;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.VCLUI.Wait, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, Datasnap.DBClient, FireDAC.Comp.DataSet,
  FireDAC.Comp.Client, Vcl.Grids, Vcl.DBGrids, JvExDBGrids, JvDBGrid,
  Vcl.Buttons, Vcl.StdCtrls, Vcl.ComCtrls, DateUtils, Datasnap.Provider,
  FireDAC.Phys.MSSQLDef, FireDAC.Phys.ODBCBase, FireDAC.Phys.MSSQL,
  JvComponentBase, JvDBGridExport, ShellAPI, MidasLib;

type
  TForm1 = class(TForm)
    dtpFecha: TDateTimePicker;
    lblFecha: TLabel;
    spbMakeConsulta: TSpeedButton;
    FDSIGLOConnection: TFDConnection;
    FDqrySIGLO: TFDQuery;
    lblDescripcion: TLabel;
    lblTitulo: TLabel;
    FDPhysMSSQLDriverLink1: TFDPhysMSSQLDriverLink;
    dsConsulta: TDataSource;
    cdsConsulta: TClientDataSet;
    dspConsulta: TDataSetProvider;
    sbExportarCSV: TSpeedButton;
    JvDBGridCSVExport1: TJvDBGridCSVExport;
    jvdbgConsulta: TJvDBGrid;
    sdConsulta: TSaveDialog;
    procedure spbMakeConsultaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure sbExportarCSVClick(Sender: TObject);
  private    { Private declarations }
  public    { Public declarations }
  end;

var
  Form1: TForm1;


implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin

  dtpFecha.MaxDate := Now;
  dtpFecha.DateTime := Now;

  FDSIGLOConnection.Connected := False;
  FDSIGLOConnection.Params.Clear;
  FDSIGLOConnection.Params.Add('DriverID=MSSQL');
  FDSIGLOConnection.Params.Add('Server=10.28.5.99');
  FDSIGLOConnection.Params.Add('Database=SIGLO');
  FDSIGLOConnection.Params.Add('User_Name=AUTO');
  FDSIGLOConnection.Params.Add('Password=autohw');
  FDSIGLOConnection.Connected := True;

end;

procedure TForm1.spbMakeConsultaClick(Sender: TObject);
var
  dAntes, DDespues: string;
  dateAux: TDateTime;
begin
  dAntes := FormatDateTime('mm/dd/yyyy', dtpFecha.DateTime);
  dateAux := IncDay(dtpFecha.DateTime, 1);
  DDespues := FormatDateTime('mm/dd/yyyy', dateAux);

  cdsConsulta.Active := False;
  FDqrySIGLO.Close;
  FDqrySIGLO.SQL.Text := 'SELECT fecha, sid, tubo, localiza, cuando FROM MCT WHERE cuando>=:ant AND cuando<:des ORDER BY cuando';
  FDqrySIGLO.Params.ParamByName('ant').AsString := dAntes;
  FDqrySIGLO.Params.ParamByName('des').AsString := DDespues;
  FDqrySIGLO.Open;

  cdsConsulta.Active := True;
  FDqrySIGLO.Close;

end;

procedure TForm1.sbExportarCSVClick(Sender: TObject);
begin

  if cdsConsulta.Active then
  begin
    if sdConsulta.Execute then
    begin
      JvDBGridCSVExport1.Grid := jvdbgConsulta;
      JvDBGridCSVExport1.ExportSeparator := esSemiColon;
      JvDBGridCSVExport1.FileName := sdConsulta.FileName;
      JvDBGridCSVExport1.ExportGrid;
    end;

    ShellExecute(Handle, 'open', PChar(sdConsulta.Filename), nil, nil, SW_SHOWNORMAL);
  end;

end;

end.


object Form1: TForm1
  Left = 0
  Top = 0
  BorderStyle = bsSingle
  Caption = 'Lista '
  ClientHeight = 550
  ClientWidth = 900
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -15
  Font.Name = 'Arial'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 17
  object lblFecha: TLabel
    Left = 252
    Top = 148
    Width = 109
    Height = 17
    Caption = 'Selecionar fecha'
  end
  object spbMakeConsulta: TSpeedButton
    Left = 559
    Top = 141
    Width = 90
    Height = 30
    Cursor = crHandPoint
    Caption = 'Buscar'
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 10903808
    Font.Height = -15
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    OnClick = spbMakeConsultaClick
  end
  object lblDescripcion: TLabel
    Left = 38
    Top = 68
    Width = 410
    Height = 17
    Caption = 'Seleciona una fecha y pulsa en "Buscar" para obtener el listado'
  end
  object lblTitulo: TLabel
    Left = 8
    Top = 8
    Width = 209
    Height = 32
    Caption = 'Listado por fecha'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = 10903808
    Font.Height = -27
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object sbExportarCSV: TSpeedButton
    Left = 777
    Top = 167
    Width = 115
    Height = 30
    Cursor = crHandPoint
    Caption = 'Exportar CSV'
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGreen
    Font.Height = -15
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    OnClick = sbExportarCSVClick
  end
  object dtpFecha: TDateTimePicker
    Left = 367
    Top = 144
    Width = 186
    Height = 25
    Date = 44588.000000000000000000
    Time = 0.581028553242504100
    TabOrder = 0
  end
  object jvdbgConsulta: TJvDBGrid
    Left = 0
    Top = 203
    Width = 900
    Height = 347
    Align = alBottom
    DataSource = dsConsulta
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -15
    TitleFont.Name = 'Arial'
    TitleFont.Style = []
    SelectColumnsDialogStrings.Caption = 'Select columns'
    SelectColumnsDialogStrings.OK = '&OK'
    SelectColumnsDialogStrings.NoSelectionWarning = 'At least one column must be visible!'
    EditControls = <>
    RowsHeight = 21
    TitleRowHeight = 21
  end
  object FDSIGLOConnection: TFDConnection
    LoginPrompt = False
    Left = 152
    Top = 152
  end
  object FDqrySIGLO: TFDQuery
    Connection = FDSIGLOConnection
    Left = 24
    Top = 152
  end
  object FDPhysMSSQLDriverLink1: TFDPhysMSSQLDriverLink
    ODBCDriver = 'SQL Server Native Client 11.0'
    Left = 800
    Top = 40
  end
  object dsConsulta: TDataSource
    DataSet = cdsConsulta
    Left = 72
    Top = 96
  end
  object cdsConsulta: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'dspConsulta'
    Left = 152
    Top = 96
  end
  object dspConsulta: TDataSetProvider
    DataSet = FDqrySIGLO
    Left = 216
    Top = 104
  end
  object JvDBGridCSVExport1: TJvDBGridCSVExport
    Caption = 'Exporting to CSV/Text...'
    Grid = jvdbgConsulta
    ExportSeparator = esSemiColon
    Left = 712
    Top = 88
  end
  object sdConsulta: TSaveDialog
    DefaultExt = 'csv'
    Filter = 'CSV (*.csv;*.txt)|*.csv;*.txt'
    Left = 632
    Top = 88
  end
end
